package ultimate.javands.injector;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import static java.nio.file.Files.readAllBytes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import static java.nio.file.Paths.get;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Zorn
 */
public class UltimateJavaNDSInjector {
    static String userID;
    static String userPDC;
    static String userTitle;
    static String SELECTED_TITLEID;
    static String SELECTED_TITLEKEY;
    
    public final static ArrayList<GameData> GAMES;
    
    public final static String JNUSTOOLSCONFIG = 
        "http://ccs.cdn.wup.shop.nintendo.net/ccs/download\n" + 
        "%COMMONKEY%\n" +
        "https://tagaya.wup.shop.nintendo.net/tagaya/versionlist/EUR/EU/latest_version\n" +
        "https://tagaya-wup.cdn.nintendo.net/tagaya/versionlist/EUR/EU/list/%d.versionlist"
    ;
    /**
     * testbed for commands
     * @param args
     */
    public static void main(String[] args) {
        GetMissingPDCFlds(); 
    }
    static {
        GAMES = new ArrayList<>();
        //USA
        GAMES.add(new GameData("Wario Ware: Touched!", "USA", "00050000101a1f00", "efaa", "DAGE", "WarioWare Touched! [DAGE01]", "WW"));
        GAMES.add(new GameData("Mario & Luigi: Partners in Time", "USA", "00050000101a2200", "1a51", "DAHE", "Mario & Luigi Partners in Time [DAHE01]", "ML"));
        GAMES.add(new GameData("New Super Mario Bros.", "USA", "0005000010195a00", "9fe3", "DADE", "New Super Mario Bros. [DADE01]", "MB"));
        //GAMES.add(new GameData("Yoshi Touch and Go", "USA", "", "", "", "YT"));
        GAMES.add(new GameData("Mario Kart DS", "USA", "0005000010195700", "e766", "DACE", "Mario Kart DS [DACE01]", "MK"));
        GAMES.add(new GameData("The Legend of Zelda: Phantom Hourglass", "USA", "00050000101c3700", "2de3", "DATE", "The Legend of Zelda Phantom Hourglass [DATE01]", "ZP"));
        GAMES.add(new GameData("Kirby's Squeak Squad", "USA", "00050000101a5600", "fdfe", "DAKE", "Kirby Squeak Squad [DAKE01]", "KS"));
        GAMES.add(new GameData("Brain Age: Train Your Brain In Minutes A Day", "USA", "0005000010179b00", "cdee", "DAAE", "Brain Age Train Your Brain In Minutes A Day [DAAE01]", "BA"));
        GAMES.add(new GameData("Fire Emblem: Shadow Dragon", "USA", "00050000101b3800", "46b0", "DANE", "Fire Emblem Shadow Dragon [DANE01]", "FE"));
        GAMES.add(new GameData("Yoshi's Island DS", "USA", "0005000010198900", "b9e2", "DAEE", "Yoshi’s Island DS [DAEE01]", "YI"));
        GAMES.add(new GameData("The Legend of Zelda Spirit Tracks", "USA", "00050000101b8c00", "c37a", "DARE", "The Legend of Zelda Spirit Tracks [DARE01]", "ZS"));
        //EUR
        GAMES.add(new GameData("Wario Ware: Touched!", "EUR", "00050000101a2000", "1337", "DAGP", "WarioWare Touched! [DAGP01]", "WW"));
        GAMES.add(new GameData("Mario & Luigi: Partners in Time", "EUR", "00050000101a2300", "095a", "DAHP", "Mario & Luigi Partners in Time [DAHP01]", "ML"));
        GAMES.add(new GameData("New Super Mario Bros.", "EUR", "0005000010195b00", "54b4", "DADP", "NEW SUPER MARIO BROS. [DADP01]", "MB"));
        GAMES.add(new GameData("Yoshi Touch and Go", "EUR", "0005000010179f00", "0942", "DABP", "Yoshi Touch & Go [DABP01]", "YT"));
        GAMES.add(new GameData("Mario Kart DS", "EUR", "0005000010195800", "67df", "DACP", "MARIO KART DS [DACP01]", "MK"));
        GAMES.add(new GameData("The Legend of Zelda: Phantom Hourglass", "EUR", "00050000101c3800", "5069", "DATP", "The Legend of Zelda Phantom Hourglass [DATP01]", "ZP"));
        //GAMES.add(new GameData("Kirby's Squeak Squad", "EUR", "", "", "", "KS"));
        //GAMES.add(new GameData("Brain Age: Train Your Brain In Minutes A Day", "EUR", "", "", "", "BA"));
        //GAMES.add(new GameData("Fire Emblem: Shadow Dragon", "EUR", "", "", "", "FE"));
        GAMES.add(new GameData("Yoshi's Island DS", "EUR", "0005000010198a00", "40d8", "DAEP", "YOSHI’S ISLAND DS [DAEP01]", "YI"));
        GAMES.add(new GameData("The Legend of Zelda Spirit Tracks", "EUR", "00050000101b8d00", "ea37", "DARP", "The Legend of Zelda Spirit Tracks [DARP01]", "ZS"));
    
    }
    private static void GetMissingPDCFlds() {
        Map<String, String> tiks = new HashMap();
        for (GameData gameData : GAMES) {
            if (gameData.ProductCode.equals("")) {
                SELECTED_TITLEID = gameData.TitleID;
                try {
                    System.out.print("Enter tik for " + gameData.Title + " " + gameData.Region + ": ");
                    SELECTED_TITLEKEY = GetInput();
                } catch (IOException ex) {
                }
                try {
                    runJNUSTool("/code/.*");
                } catch (Exception ex) {
                    Logger.getLogger(UltimateJavaNDSInjector.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        tiks.entrySet().stream().forEach((game) -> {
            String key = game.getKey();
            String value = game.getValue();
            System.out.println(key + ";" + value);
        }); //runNUSPacker();
        
        ArrayList<String> files = new ArrayList<>(Arrays.asList(new File("Tools").list()));
        files.stream().forEach((file) -> {
            if (file.endsWith("]")) {
                System.out.println("\"" + file.substring(file.length()-7, file.length()-3) + "\", \"" + file + "\"");
            }
        });
    }

    private static String GetInput() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String s = br.readLine();
        return s;
    }

    static void zipRom(String filename) throws Exception {
        try {
            final ProcessBuilder pb = new ProcessBuilder(new File("Tools/7za.exe").getAbsolutePath(), 
                    "a", "NUSPACKER/input/content/0010/rom.zip", filename)
                    .inheritIO()
                    .directory(new File("Tools"));

            RunProcess(pb);
        } catch (Exception e)
        {
            //try to use 7z from the PATH
            try {
                final ProcessBuilder pb = new ProcessBuilder("7z", 
                        "a", "NUSPACKER/input/content/0010/rom.zip", filename)
                        .inheritIO()
                        .directory(new File("Tools"));

                RunProcess(pb);
                
            } catch (Exception e2) {
                
                JOptionPane.showMessageDialog(new JFrame(), "You seem to not be "+
                        "on Windows, please install 7Zip and make sure 7z is "
                        + "on your PATH.", "Error",
                        JOptionPane.ERROR_MESSAGE);
                throw e2;
            }
        }
    }

    static void runPng2TgaCmd(String filename, int width, int height, int bpp) throws Exception {
        final ProcessBuilder pb = new ProcessBuilder(new File("Tools/png2tgacmd.exe").getAbsolutePath(), 
                "-i", filename, 
                "-o", "meta", 
                "--width=" + width, "--height=" + height, 
                "--tga-bpp=" + bpp, "--tga-compression=none")
                .inheritIO()
                .directory(new File("Tools"));
        
        RunProcess(pb);
    }

    static void runNUSPacker(String path, String CommonKey) throws Exception {
        ProcessBuilder pb = new ProcessBuilder("java", "-jar",
                "NUSPacker.jar", "-in", "input", "-out", path,
                "-encryptKeyWith", CommonKey)
                .inheritIO()
                .directory(new File("Tools/NUSPACKER"));
        
        RunProcess(pb);
        System.out.println("Completed NusPacker.");
    }

    static void runJNUSTool(String path) throws Exception {
        ProcessBuilder pb = new ProcessBuilder("java", "-jar",
                "JNUSTool.jar", SELECTED_TITLEID, SELECTED_TITLEKEY, "-file", path)
                .inheritIO()
                .directory(new File("Tools/JNUSTOOL"));

        RunProcess(pb);
        System.out.println("Completed " + path);
    }

    private static void RunProcess(ProcessBuilder pb) throws Exception {
        try {
            final Process p = pb.start();
            final int exitStatus = p.waitFor();
            System.out.println("Processed finished with status: " + exitStatus);
        } catch (IOException | InterruptedException e) {
            throw e;
        }
    }
    
    public static class GameData {
        public String Title, Region, TitleID, TitleKeyHINT, ProductCode, JNUSToolFolder, Acronym;
        public GameData(String title, String reg, String tid, String tikh, String pdc, String fld, String acro)
        {
            this.Title = title;
            this.Region = reg;
            this.TitleID = tid;
            this.TitleKeyHINT = tikh;
            this.ProductCode = pdc;
            this.JNUSToolFolder = fld;
            this.Acronym = acro;
        }
        public String getTitleKeyFileName()
        {
            //if file exists, get from file, otherwise check titlekey input field
            //String titSplit[] = Title.split(" ");
            //String letter1 = titSplit[0].substring(0,1).toUpperCase();
            //String letter2 = (titSplit[1].equals("&") ? titSplit[2] : titSplit[1]).substring(0,1).toUpperCase();
            return this.Acronym + this.Region.substring(0, 2) + "TITLEKEY";
        }

        public String getTitleKey()
        {
            String tik = "";
            try {
                tik = new String(readAllBytes(get("Storage/" + getTitleKeyFileName()))).substring(0, 32);
            } catch (IOException ex) {
                //doesn't exist yet
            }
            return tik;
        }
        @Override
        public String toString() {
            return Title + " (" + Region + ")";
        }
    }
}
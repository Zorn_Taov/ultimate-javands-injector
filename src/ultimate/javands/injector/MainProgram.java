/*
 * The MIT License
 *
 * Copyright 2017 Zorn.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package ultimate.javands.injector;

import java.awt.Desktop;
import static java.awt.Frame.getFrames;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.DirectoryStream;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import static java.nio.file.Files.readAllBytes;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import static java.nio.file.StandardCopyOption.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import ultimate.javands.injector.UltimateJavaNDSInjector.GameData;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.prefs.Preferences;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import static java.nio.file.Paths.get;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.Timer;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import net.npe.tga.TGAReader;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import static java.nio.file.Paths.get;
import static java.nio.file.Paths.get;
import static java.nio.file.Paths.get;
import javax.imageio.ImageIO;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import static java.nio.file.Paths.get;
import static java.nio.file.Paths.get;
import static java.nio.file.Paths.get;
import static java.nio.file.Paths.get;
import net.npe.tga.TGAWriter;

/**
 *
 * @author Zorn
 */
public class MainProgram extends javax.swing.JFrame {

    private boolean renameAll = false;
    private int lastState = 0;
    private int pX;
    private int pY;
    int frameWidth, frameHeight;
    Icon tvImageBackup, drcImageBackup, iconImageBackup;
    /**
     * Creates new form MainProgram
     */
    public MainProgram() {
        initComponents();
        gameTitleBox.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void changedUpdate(DocumentEvent e) {
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                try {
                    bootTvLabel.setText(e.getDocument().getText(0, e.getDocument().getLength()));
                    bootDrcLabel.setText(e.getDocument().getText(0, e.getDocument().getLength()));
                } catch (BadLocationException ex) {
                    Logger.getLogger(MainProgram.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                try {
                    bootTvLabel.setText(e.getDocument().getText(0, e.getDocument().getLength()));
                    bootDrcLabel.setText(e.getDocument().getText(0, e.getDocument().getLength()));
                } catch (BadLocationException ex) {
                    Logger.getLogger(MainProgram.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
        });
        tvImageBackup = bootTVIcon.getIcon();
        drcImageBackup = bootDrcIcon.getIcon();
        iconImageBackup = iconTexIcon.getIcon();
        frameWidth = 450;
        frameHeight = 339;
        timerWidth = new Timer(1, new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    java.awt.Frame frame = getFrames()[0];
                    frame.setSize(frame.getWidth()+drawDelta, frame.getHeight());
                    if (frame.getWidth() >= 1765) {
                        frame.setSize(1765, frame.getHeight());
                        timerWidth.stop();
                    } else if (frame.getWidth() <= frameWidth) {
                        frame.setSize(frameWidth, frame.getHeight());
                        timerWidth.stop();
                    }
                    repaint();
                }
            });
        timerHeight = new Timer(1, new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    java.awt.Frame frame = getFrames()[0];
                    frame.setSize(frame.getWidth(), frame.getHeight()+drawDelta);
                    if (frame.getHeight() >= 880) {
                        frame.setSize(frame.getWidth(), 880);
                        timerHeight.stop();
                    } else if (frame.getHeight() <= frameHeight) {
                        frame.setSize(frame.getWidth(), frameHeight);
                        timerHeight.stop();
                    }
                    repaint();
                }
            });
        try {
            Files.createDirectory(get("Files"));
            Files.createDirectory(get("Install"));
        } catch (IOException ex) {
            if (!(ex instanceof java.nio.file.FileAlreadyExistsException)) {
                Logger.getLogger(MainProgram.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        progressBar.setString("");
        //load wiiu common key from file if it exists
        commonKeyBox.setText(getWiiUCommonKey());
        //load wario tik from file if it exists, since it's first in the list
        Preferences prefs = Preferences.userRoot().node(getClass().getName());
        gameSelecter.setSelectedIndex(prefs.getInt("LAST_USED_BASEROM", 0));
        titleKeyBox.setText(gameSelecter.getItemAt(gameSelecter.getSelectedIndex()).getTitleKey());
        gameTitleBox.setText(prefs.get("LAST_USED_TITLE", gameTitleBox.getText()));
        productCodeBox.setText(prefs.get("LAST_USED_PDC",   productCodeBox.getText()));
        titleIdBox.setText(prefs.get("LAST_USED_TID",   titleIdBox.getText()));
        fileBox.setText(prefs.get("LAST_USED_FILE",  fileBox.getText()));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        textureFrame = new javax.swing.JFrame();
        tabbedPanel = new javax.swing.JTabbedPane();
        bootTvTexPanel = new javax.swing.JLayeredPane();
        bootTVIcon = new javax.swing.JLabel();
        bootTvLabel = new javax.swing.JLabel();
        bootTvImport = new javax.swing.JLabel();
        bootDrcTexPanel = new javax.swing.JLayeredPane();
        bootDrcIcon = new javax.swing.JLabel();
        bootDrcLabel = new javax.swing.JLabel();
        bootDrcImport = new javax.swing.JLabel();
        iconTexPanel = new javax.swing.JLayeredPane();
        iconTexIcon = new javax.swing.JLabel();
        iconTexImport = new javax.swing.JLabel();
        importButton1 = new javax.swing.JButton();
        importButton = new javax.swing.JButton();
        progressBar = new javax.swing.JProgressBar();
        controls = new javax.swing.JPanel();
        openFolderButton = new javax.swing.JButton();
        commonKeyBox = new javax.swing.JTextField();
        commonKeyLabel = new javax.swing.JLabel();
        titleKeyLabel = new javax.swing.JLabel();
        titleKeyBox = new javax.swing.JTextField();
        gameSelecter = new javax.swing.JComboBox<>();
        baseRomLabel = new javax.swing.JLabel();
        startButton = new javax.swing.JButton();
        gameInfoPanel = new javax.swing.JPanel();
        gameTitleLabel = new javax.swing.JLabel();
        gameTitleBox = new javax.swing.JTextField();
        titleIdLabel = new javax.swing.JLabel();
        titleIdBox = new javax.swing.JTextField();
        productCodeLabel = new javax.swing.JLabel();
        productCodeBox = new javax.swing.JTextField();
        fileSelectButton = new javax.swing.JButton();
        fileBox = new javax.swing.JTextField();
        resumeButton = new javax.swing.JButton();
        repackButton = new javax.swing.JButton();
        expandButton = new javax.swing.JButton();

        textureFrame.setTitle("Texture Stitcher");
        textureFrame.setResizable(false);
        textureFrame.setSize(new java.awt.Dimension(1305, 799));
        textureFrame.addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                textureFrameWindowClosing(evt);
            }
        });

        bootTvTexPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        bootTVIcon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/bootTvTex.png"))); // NOI18N
        bootTvTexPanel.setLayer(bootTVIcon, javax.swing.JLayeredPane.PALETTE_LAYER);
        bootTvTexPanel.add(bootTVIcon, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        bootTvLabel.setFont(new java.awt.Font("Arial", 0, 30)); // NOI18N
        bootTvLabel.setText("<Target");
        bootTvLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        bootTvTexPanel.setLayer(bootTvLabel, javax.swing.JLayeredPane.MODAL_LAYER);
        bootTvTexPanel.add(bootTvLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 330, -1, -1));
        bootTvTexPanel.add(bootTvImport, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 250, 400, 300));

        tabbedPanel.addTab("bootTvTex", bootTvTexPanel);

        bootDrcTexPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        bootDrcIcon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/bootDrcTex.png"))); // NOI18N
        bootDrcTexPanel.setLayer(bootDrcIcon, javax.swing.JLayeredPane.PALETTE_LAYER);
        bootDrcTexPanel.add(bootDrcIcon, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        bootDrcLabel.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        bootDrcLabel.setText("<Target");
        bootDrcLabel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        bootDrcTexPanel.setLayer(bootDrcLabel, javax.swing.JLayeredPane.MODAL_LAYER);
        bootDrcTexPanel.add(bootDrcLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 221, -1, -1));
        bootDrcTexPanel.add(bootDrcImport, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 160, 280, 210));

        tabbedPanel.addTab("bootDrcTex", bootDrcTexPanel);

        iconTexPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        iconTexIcon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Resources/iconTex.png"))); // NOI18N
        iconTexPanel.setLayer(iconTexIcon, javax.swing.JLayeredPane.PALETTE_LAYER);
        iconTexPanel.add(iconTexIcon, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));
        iconTexPanel.add(iconTexImport, new org.netbeans.lib.awtextra.AbsoluteConstraints(4, 9, 120, 92));

        tabbedPanel.addTab("iconTex", iconTexPanel);

        importButton1.setText("Export All");
        importButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                importButton1ActionPerformed(evt);
            }
        });

        importButton.setText("Import Icon");
        importButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                importButtonActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout textureFrameLayout = new org.jdesktop.layout.GroupLayout(textureFrame.getContentPane());
        textureFrame.getContentPane().setLayout(textureFrameLayout);
        textureFrameLayout.setHorizontalGroup(
            textureFrameLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(textureFrameLayout.createSequentialGroup()
                .addContainerGap()
                .add(textureFrameLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(tabbedPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(textureFrameLayout.createSequentialGroup()
                        .add(importButton)
                        .add(11, 11, 11)
                        .add(importButton1)
                        .add(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        textureFrameLayout.setVerticalGroup(
            textureFrameLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, textureFrameLayout.createSequentialGroup()
                .addContainerGap()
                .add(textureFrameLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(importButton)
                    .add(importButton1))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(tabbedPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Ultimate JavaNDS Injector");
        setResizable(false);

        progressBar.setMaximum(20);
        progressBar.setStringPainted(true);

        controls.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        openFolderButton.setText("Texture Folder");
        openFolderButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openFolderButtonActionPerformed(evt);
            }
        });

        commonKeyBox.setToolTipText("");
        commonKeyBox.setName("CommonKeyField"); // NOI18N

        commonKeyLabel.setText("WiiU Common Key");

        titleKeyLabel.setText("BaseRom TitleKey");

        titleKeyBox.setToolTipText("");
        titleKeyBox.setName("CommonKeyField"); // NOI18N

        gameSelecter.setModel(getComboModel());
        gameSelecter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                gameSelecterActionPerformed(evt);
            }
        });

        baseRomLabel.setText("BaseRom");

        startButton.setText("Start");
        startButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startButtonActionPerformed(evt);
            }
        });

        gameInfoPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        gameInfoPanel.setMaximumSize(new java.awt.Dimension(407, 333));

        gameTitleLabel.setText("Game Title");

        gameTitleBox.setToolTipText("");
        gameTitleBox.setName("CommonKeyField"); // NOI18N

        titleIdLabel.setText("Title ID");

        titleIdBox.setToolTipText("");
        titleIdBox.setName("CommonKeyField"); // NOI18N

        productCodeLabel.setText("Product Code");

        productCodeBox.setToolTipText("");
        productCodeBox.setName("CommonKeyField"); // NOI18N

        fileSelectButton.setText("Select DS Rom");
        fileSelectButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fileSelectButtonActionPerformed(evt);
            }
        });

        fileBox.setToolTipText("");
        fileBox.setMaximumSize(new java.awt.Dimension(14, 21));

        org.jdesktop.layout.GroupLayout gameInfoPanelLayout = new org.jdesktop.layout.GroupLayout(gameInfoPanel);
        gameInfoPanel.setLayout(gameInfoPanelLayout);
        gameInfoPanelLayout.setHorizontalGroup(
            gameInfoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(gameInfoPanelLayout.createSequentialGroup()
                .add(12, 12, 12)
                .add(gameInfoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(fileSelectButton)
                    .add(productCodeLabel)
                    .add(titleIdLabel)
                    .add(gameTitleLabel))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(gameInfoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(gameTitleBox)
                    .add(titleIdBox)
                    .add(productCodeBox)
                    .add(fileBox, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        gameInfoPanelLayout.setVerticalGroup(
            gameInfoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(gameInfoPanelLayout.createSequentialGroup()
                .add(12, 12, 12)
                .add(gameInfoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(gameInfoPanelLayout.createSequentialGroup()
                        .add(3, 3, 3)
                        .add(gameTitleLabel))
                    .add(gameTitleBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(6, 6, 6)
                .add(gameInfoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(gameInfoPanelLayout.createSequentialGroup()
                        .add(3, 3, 3)
                        .add(titleIdLabel))
                    .add(titleIdBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(6, 6, 6)
                .add(gameInfoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(gameInfoPanelLayout.createSequentialGroup()
                        .add(3, 3, 3)
                        .add(productCodeLabel))
                    .add(productCodeBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(6, 6, 6)
                .add(gameInfoPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(fileSelectButton)
                    .add(gameInfoPanelLayout.createSequentialGroup()
                        .add(3, 3, 3)
                        .add(fileBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        resumeButton.setText("Resume from Error");
        resumeButton.setEnabled(false);
        resumeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resumeButtonActionPerformed(evt);
            }
        });

        repackButton.setText("Repack");
        repackButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                repackButtonActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout controlsLayout = new org.jdesktop.layout.GroupLayout(controls);
        controls.setLayout(controlsLayout);
        controlsLayout.setHorizontalGroup(
            controlsLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(controlsLayout.createSequentialGroup()
                .addContainerGap()
                .add(controlsLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(gameInfoPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(controlsLayout.createSequentialGroup()
                        .add(controlsLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(commonKeyLabel)
                            .add(titleKeyLabel)
                            .add(baseRomLabel))
                        .add(21, 21, 21)
                        .add(controlsLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(gameSelecter, 0, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(titleKeyBox)
                            .add(commonKeyBox)))
                    .add(controlsLayout.createSequentialGroup()
                        .add(openFolderButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(resumeButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(repackButton)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(startButton)))
                .addContainerGap())
        );
        controlsLayout.setVerticalGroup(
            controlsLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(controlsLayout.createSequentialGroup()
                .addContainerGap()
                .add(controlsLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(gameSelecter, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(baseRomLabel))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(controlsLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(titleKeyBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(titleKeyLabel))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(controlsLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(commonKeyBox, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(commonKeyLabel))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(gameInfoPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 125, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(controlsLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(startButton)
                    .add(openFolderButton)
                    .add(repackButton)
                    .add(resumeButton))
                .addContainerGap())
        );

        commonKeyLabel.getAccessibleContext().setAccessibleName("");

        expandButton.setText(">");
        expandButton.setMargin(new java.awt.Insets(2, 2, 2, 2));
        expandButton.setMaximumSize(new java.awt.Dimension(41, 10));
        expandButton.setMinimumSize(new java.awt.Dimension(41, 10));
        expandButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                expandButtonActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(progressBar, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(layout.createSequentialGroup()
                        .add(controls, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(expandButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 21, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(expandButton, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(controls, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(progressBar, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 25, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void fileSelectButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fileSelectButtonActionPerformed
        Preferences prefs = Preferences.userRoot().node(getClass().getName());
        JFileChooser jfc = new JFileChooser(prefs.get("LAST_USED_FOLDER",
    new File(".").getAbsolutePath()));
        jfc.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                if (f.isDirectory()) {
                    return true;
                }
                String s = f.getName(), ext = "";
                int i = s.lastIndexOf('.');
                if (i > 0 &&  i < s.length() - 1) {
                    ext = s.substring(i+1).toLowerCase();
                }
                return ext.equals("nds");
            }

            @Override
            public String getDescription() {
                return "NDS Game";
            }
        });
        if(jfc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            File file = jfc.getSelectedFile();
            fileBox.setText(file.getPath());
            prefs.put("LAST_USED_FOLDER", jfc.getSelectedFile().getParent());
        } 
    }//GEN-LAST:event_fileSelectButtonActionPerformed

    private void gameSelecterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_gameSelecterActionPerformed
        //load up titlekey from file if it exists into 
        titleKeyBox.setText(gameSelecter.getItemAt(gameSelecter.getSelectedIndex()).getTitleKey());
    }//GEN-LAST:event_gameSelecterActionPerformed

    private void openFolderButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openFolderButtonActionPerformed
        try {
            Desktop.getDesktop().open(new File("./Files"));
        } catch (IOException ex) {
            Logger.getLogger(MainProgram.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_openFolderButtonActionPerformed

    private void startButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startButtonActionPerformed
        RunInjector(0);
    }//GEN-LAST:event_startButtonActionPerformed

    private void resumeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resumeButtonActionPerformed
        RunInjector(--lastState);
    }//GEN-LAST:event_resumeButtonActionPerformed

    private void repackButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_repackButtonActionPerformed
        lastState = 15;
        RunInjector(15);
    }//GEN-LAST:event_repackButtonActionPerformed
        private boolean drawIn = false;
        private Timer timerWidth, timerHeight;
        private int drawDelta = 10;
    private void expandButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_expandButtonActionPerformed
        drawIn = !drawIn;
        expandButton.setText((drawIn ? "<" : ">"));
        textureFrame.setVisible(drawIn);
    }//GEN-LAST:event_expandButtonActionPerformed

    private void importButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_importButtonActionPerformed
        Preferences prefs = Preferences.userRoot().node(getClass().getName());
        JFileChooser jfc = new JFileChooser(prefs.get("LAST_USED_IMAGE_FOLDER",
            new File(".").getAbsolutePath()));
        jfc.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                if (f.isDirectory()) {
                    return true;
                }
                String s = f.getName(), importExt = "";
                int i = s.lastIndexOf('.');
                if (i > 0 &&  i < s.length() - 1) {
                    importExt = s.substring(i+1).toLowerCase();
                }
                return importExt.equals("png") || importExt.equals("tga");
            }

            @Override
            public String getDescription() {
                return "ImageFiles (png/tga)";
            }
        });
        if(jfc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            try {
                File file = jfc.getSelectedFile();
                prefs.put("LAST_USED_IMAGE_FOLDER", jfc.getSelectedFile().getParent());
                byte[] buffer;
                try (FileInputStream fis = new FileInputStream(file.getPath())) {
                    buffer = new byte[fis.available()];
                    fis.read(buffer);
                }
                String s = file.getName(), importExt = "";
                int i = s.lastIndexOf('.');
                if (i > 0 &&  i < s.length() - 1) {
                    importExt = s.substring(i+1).toLowerCase();
                }
                BufferedImage image = null;
                if (importExt.equals("tga")) {
                    int [] pixels = TGAReader.read(buffer, TGAReader.ARGB);
                    int width = TGAReader.getWidth(buffer);
                    int height = TGAReader.getHeight(buffer);
                    image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
                    image.setRGB(0, 0, width, height, pixels, 0, width);
                }
                else if (importExt.equals("png")) {
                    image = ImageIO.read(file);
                }
                if(image == null) throw new IOException();
                ImageIcon iconTv = new ImageIcon(image.getScaledInstance(bootTvImport.getWidth(), 
                        bootTvImport.getHeight(), BufferedImage.SCALE_SMOOTH));
                bootTvImport.setIcon(iconTv);
                ImageIcon iconDrc = new ImageIcon(image.getScaledInstance(bootDrcImport.getWidth(), 
                        bootDrcImport.getHeight(), BufferedImage.SCALE_SMOOTH));
                bootDrcImport.setIcon(iconDrc);
                ImageIcon iconTex = new ImageIcon(image.getScaledInstance(iconTexImport.getWidth(), 
                        iconTexImport.getHeight(), BufferedImage.SCALE_SMOOTH));
                iconTexImport.setIcon(iconTex);
            } catch (IOException ex) {
                Logger.getLogger(MainProgram.class.getName()).log(Level.SEVERE, null, ex);
            }
        } 
    }//GEN-LAST:event_importButtonActionPerformed

    private void importButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_importButton1ActionPerformed
        
        {
            BufferedImage image = new BufferedImage(bootTvTexPanel.getWidth(), bootTvTexPanel.getHeight(), BufferedImage.TYPE_INT_RGB);
            Graphics2D g = image.createGraphics();
            bootTvTexPanel.printAll(g);
            g.dispose();

            int width = 1280;
            int height = 720;
            int [] pixels = image.getRGB(0, 0, width, height, null, 0, width);
            byte [] buffer = TGAWriter.write(pixels, width, height, TGAReader.ARGB, TGAWriter.EncodeType.NONE);
            try (FileOutputStream fos = new FileOutputStream(new File("./Files/bootTvTex.tga").getAbsolutePath())) {
                fos.write(buffer);
            } catch (IOException ex) {
                Logger.getLogger(MainProgram.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        {
            BufferedImage image = new BufferedImage(bootDrcTexPanel.getWidth(), bootDrcTexPanel.getHeight(), BufferedImage.TYPE_INT_RGB);
            Graphics2D g = image.createGraphics();
            bootDrcTexPanel.printAll(g);
            g.dispose();

            int width = 854;
            int height = 480;
            int [] pixels = image.getRGB(0, 0, width, height, null, 0, width);
            byte [] buffer = TGAWriter.write(pixels, width, height, TGAReader.ARGB, TGAWriter.EncodeType.NONE);
            try (FileOutputStream fos = new FileOutputStream(new File("./Files/bootDrcTex.tga").getAbsolutePath())) {
                fos.write(buffer);
            } catch (IOException ex) {
                Logger.getLogger(MainProgram.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        {
            BufferedImage image = new BufferedImage(iconTexPanel.getWidth(), iconTexPanel.getHeight(), BufferedImage.TYPE_INT_ARGB);
            Graphics2D g = image.createGraphics();
            iconTexPanel.printAll(g);
            g.dispose();

            int width = 128;
            int height = 128;
            int [] pixels = image.getRGB(0, 0, width, height, null, 0, width);
            byte [] buffer = TGAWriter.write(pixels, width, height, TGAReader.ARGB, TGAWriter.EncodeType.NONE);
            try (FileOutputStream fos = new FileOutputStream(new File("./Files/iconTex.tga").getAbsolutePath())) {
                fos.write(buffer);
            } catch (IOException ex) {
                Logger.getLogger(MainProgram.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_importButton1ActionPerformed

    private void textureFrameWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_textureFrameWindowClosing
        drawIn = !drawIn;
        expandButton.setText((drawIn ? "<" : ">"));
    }//GEN-LAST:event_textureFrameWindowClosing

    private void RunInjector(int start) {
        // TODO add your handling code here:
        Preferences prefs = Preferences.userRoot().node(getClass().getName());
        prefs.put("LAST_USED_TITLE", gameTitleBox.getText());
        prefs.put("LAST_USED_PDC", productCodeBox.getText());
        prefs.put("LAST_USED_TID", titleIdBox.getText());
        prefs.put("LAST_USED_FILE", fileBox.getText());
        prefs.putInt("LAST_USED_BASEROM", gameSelecter.getSelectedIndex());
        
        Thread thread = new Thread(() -> {
            try{
                GameData game = gameSelecter.getItemAt(gameSelecter.getSelectedIndex());
                switch(start)
                {
                    case 0:
                        //disable input
                        if (!validateInputs(game)) {
                            return;
                        }
                        enableInput(false);
                        deleteDirectory("Tools/NUSPACKER/input/code");
                        deleteDirectory("Tools/NUSPACKER/input/content");
                        deleteDirectory("Tools/NUSPACKER/input/meta");
                        //save files
                    case 1:
                        incrementProgressBar("Saving TitleKey");
                        saveTitleKey(game);
                    case 2:
                        incrementProgressBar("Saving CommonKey");
                        saveWiiUCommonKey();
                        UltimateJavaNDSInjector.SELECTED_TITLEID = game.TitleID;
                        UltimateJavaNDSInjector.SELECTED_TITLEKEY = titleKeyBox.getText();
                        //Copying NDS file
                    case 3:
                        incrementProgressBar("Copying selected DS file.");
                        Files.copy(get(fileBox.getText()), get("Tools/WUP-N-" + game.ProductCode + ".srl"), REPLACE_EXISTING);

                    case 4:
                        incrementProgressBar("Saving JNUSTools config");
                        String JNUSConfig = UltimateJavaNDSInjector.JNUSTOOLSCONFIG.replace("%COMMONKEY%", getWiiUCommonKey());
                        try(  PrintWriter out = new PrintWriter( "Tools/JNUSTOOL/config" )  ){
                            out.println( JNUSConfig );
                        } catch (FileNotFoundException ex) {
                            Logger.getLogger(MainProgram.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        //start JNUSTools
                        //download ALL files, why not
                    case 5:
                        incrementProgressBar("Getting all files With JNUSTools");
                        UltimateJavaNDSInjector.runJNUSTool(".*");
                        //move files and folders
                    case 6:
                        incrementProgressBar("Moving code");
                        Thread.sleep(100);
                        copyFileTree(get("Tools/JNUSTOOL/"+game.JNUSToolFolder+"/code"), get("Tools/NUSPACKER/input/code"));
                    case 7:
                        incrementProgressBar("Moving content");
                        Thread.sleep(100);
                        copyFileTree(get("Tools/JNUSTOOL/"+game.JNUSToolFolder+"/content"), get("Tools/NUSPACKER/input/content"));
                    case 8:
                        incrementProgressBar("Moving meta");
                        Thread.sleep(100);
                        copyFileTree(get("Tools/JNUSTOOL/"+game.JNUSToolFolder+"/meta"), get("Tools/NUSPACKER/input/meta"));

                        //pack nds
                    case 9:
                        incrementProgressBar("Deleting old rom.zip");
                        Files.deleteIfExists(get("Tools/NUSPACKER/input/content/0010/rom.zip"));
                    case 10:
                        incrementProgressBar("7z-ing new rom.zip");
                        UltimateJavaNDSInjector.zipRom("WUP-N-" + game.ProductCode + ".srl");
                        Files.deleteIfExists(get("Tools/WUP-N-"+game.ProductCode+".srl"));

                        //save XML's
                    case 11:
                        incrementProgressBar("Saving app.xml");
                        try {
                            File appxmlpath = new File("Tools/NUSPACKER/input/code/app.xml");
                            DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
                            DocumentBuilder b = f.newDocumentBuilder();
                            Document appXmlDoc = b.parse(appxmlpath);
                            appXmlDoc.setXmlStandalone(true);
                            Node appTid = appXmlDoc.getElementsByTagName("title_id").item(0);
                            appTid.setTextContent("0005000010" + titleIdBox.getText() + "00");
                            TransformerFactory transformerFactory = TransformerFactory
                                    .newInstance();
                            Transformer transformer = transformerFactory.newTransformer();
                            DOMSource source = new DOMSource(appXmlDoc);
                            StreamResult result = new StreamResult(appxmlpath);
                            transformer.transform(source, result);
                        } catch (ParserConfigurationException | SAXException | TransformerException ex) {
                            Logger.getLogger(MainProgram.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    case 12:
                        incrementProgressBar("Saving meta.xml");
                        try {
                            File metaXmlPath = new File("Tools/NUSPACKER/input/meta/meta.xml");
                            DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
                            DocumentBuilder b = f.newDocumentBuilder();
                            Document metaXmlDoc = b.parse(metaXmlPath);
                            metaXmlDoc.setXmlStandalone(true);
                            XPathFactory xPathfactory = XPathFactory.newInstance();
                            XPath xpath = xPathfactory.newXPath();
                            XPathExpression longExpr = xpath.compile("//*[contains(local-name(), 'longname')]");
                            XPathExpression shortExpr = xpath.compile("//*[contains(local-name(), 'shortname')]");
                            Node appTidNode = metaXmlDoc.getElementsByTagName("title_id").item(0);
                            appTidNode.setTextContent("0005000010" + titleIdBox.getText() + "00");
                            Node appPdcNode = metaXmlDoc.getElementsByTagName("product_code").item(0);
                            appPdcNode.setTextContent("WUP-N-" + productCodeBox.getText());
                            if (!renameAll) {
                                Node longName = metaXmlDoc.getElementsByTagName("longname_en").item(0);
                                longName.setTextContent(gameTitleBox.getText());
                                Node shortName = metaXmlDoc.getElementsByTagName("shortname_en").item(0);
                                shortName.setTextContent(gameTitleBox.getText());
                            }
                            else
                            {
                                NodeList longNameNodes = (NodeList) longExpr.evaluate(metaXmlDoc, XPathConstants.NODESET);
                                for (int i = 0; i < longNameNodes.getLength(); i++) {
                                    Node longName = longNameNodes.item(i);
                                    longName.setTextContent(gameTitleBox.getText());
                                }
                                NodeList shortNameNodes = (NodeList) shortExpr.evaluate(metaXmlDoc, XPathConstants.NODESET);
                                for (int i = 0; i < shortNameNodes.getLength(); i++) {
                                    Node shortName = shortNameNodes.item(i);
                                    shortName.setTextContent(gameTitleBox.getText());
                                }

                            }
                            TransformerFactory transformerFactory = TransformerFactory
                                    .newInstance();
                            Transformer transformer = transformerFactory.newTransformer();
                            DOMSource source = new DOMSource(metaXmlDoc);
                            StreamResult result = new StreamResult(metaXmlPath);
                            transformer.transform(source, result);
                        } catch (ParserConfigurationException | SAXException | TransformerException | XPathExpressionException ex) {
                            Logger.getLogger(MainProgram.class.getName()).log(Level.SEVERE, null, ex);
                        }

                        //move or copy PNG's and TGA's
                    case 13:
                        incrementProgressBar("copying TGA's");
                        try (DirectoryStream<Path> dirStream = Files.newDirectoryStream(get("DefaultData"), "*.tga"))
                        {
                            dirStream.forEach(path ->
                            {
                                try {
                                    if(Files.exists(get("Files/").resolve(path.getFileName()), LinkOption.NOFOLLOW_LINKS))
                                        Files.copy(get("Files/").resolve(path.getFileName()), get("Tools/NUSPACKER/input/meta").resolve(path.getFileName()), REPLACE_EXISTING);
                                    else
                                        Files.copy(path, get("Tools/NUSPACKER/input/meta").resolve(path.getFileName()), REPLACE_EXISTING);
                                } catch (IOException ex) {
                                    Logger.getLogger(MainProgram.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            });
                        }

                    case 14:
                        incrementProgressBar("copying PNG's");
                        try (DirectoryStream<Path> dirStream = Files.newDirectoryStream(
                                get("DefaultData"), "*.png")) {
                            dirStream.forEach(path ->
                            {
                                try {
                                    if(Files.exists(get("Files/").resolve(path.getFileName()), LinkOption.NOFOLLOW_LINKS))
                                        Files.copy(get("Files/").resolve(path.getFileName()), get("Tools/NUSPACKER/input/content/0010/assets/textures").resolve(path.getFileName()), REPLACE_EXISTING);
                                    else
                                        Files.copy(path, get("Tools/NUSPACKER/input/content/0010/assets/textures").resolve(path.getFileName()), REPLACE_EXISTING);
                                } catch (IOException ex) {
                                    Logger.getLogger(MainProgram.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            });
                        }
                        //start NUSPacker
                    case 15:
                        incrementProgressBar("Starting NUSPacker");
                        checkDir("Tools/NUSPACKER/install");
                        UltimateJavaNDSInjector.runNUSPacker("install/injected_vc_game", commonKeyBox.getText());
                        //


                    case 16:
                        incrementProgressBar("Moving Installer");
                        checkDir("Install");
                        Files.move(get("Tools/NUSPACKER/install/injected_vc_game"), get("Install/" + gameTitleBox.getText().replaceAll("[^A-Za-z0-9 ]", "")), REPLACE_EXISTING);

                        enableInput(true);
                        resetProgressBar("Complete");
                        lastState = 0;
                        resumeButton.setEnabled(false);
                }
                
            } catch (Exception ex) {
                Logger.getLogger(MainProgram.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(new JFrame(), ex, "Error",
                        JOptionPane.ERROR_MESSAGE);
                enableInput(true);
                resumeButton.setEnabled(true);
                resetProgressBar("Error");
            }
        });
        thread.start();
    }

    private void checkDir(String path) {
        File dir = new File(path);
        if (!(dir.exists() && dir.isDirectory())) {
            dir.mkdirs();
        }
    }

    private void copyFileTree(Path src, Path dest) throws IOException{
        if (!(dest.toFile().getParentFile().exists() && dest.toFile().getParentFile().isDirectory())) {
            dest.toFile().mkdirs();
        }
        Files.walkFileTree(src, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile( Path file, BasicFileAttributes attrs ) throws IOException {
                return copy(file);
            }
            @Override
            public FileVisitResult preVisitDirectory( Path dir, BasicFileAttributes attrs ) throws IOException {
                return copy(dir);
            }
            private FileVisitResult copy( Path fileOrDir ) throws IOException {
                Files.copy( fileOrDir, dest.resolve( src.relativize( fileOrDir ) ) );
                return FileVisitResult.CONTINUE;
            }
        });
    }
    private void deleteDirectory(String path) throws IOException {
        if(Files.exists(get(path)))
            Files.walkFileTree(get(path), new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                    System.out.println(exc.toString());
                    return FileVisitResult.CONTINUE;
                }
                
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
                    throws IOException
                {
                    Files.delete(file);
                    return FileVisitResult.CONTINUE;
                }
                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException e)
                    throws IOException
                {
                    if (e == null) {
                        Files.delete(dir);
                        return FileVisitResult.CONTINUE;
                    } else {
                        // directory iteration failed
                        throw e;
                    }
                }
            });
    }

    private boolean validateInputs(GameData game) {
        if (titleKeyBox.getText().equals("")) {                                                                               
            JOptionPane.showMessageDialog(null, "Title Key missing.");
            return false;
        } else if(!titleKeyBox.getText().substring(0,4).equals(game.TitleKeyHINT)){
            JOptionPane.showMessageDialog(null, "Title Key incorrect.");
            return false;
        }
        if (commonKeyBox.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Common Key missing.");
            return false;
        }
        if (gameTitleBox.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "No Title given.");
            return false;
        }
        if (productCodeBox.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "No Product Code given.");
            return false;
        } else if(productCodeBox.getText().length() != 4){
            JOptionPane.showMessageDialog(null, "Product Code not length of 4.");
            return false;
        }
        if (titleIdBox.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "No TitleID given.");
            return false;
        } else if(titleIdBox.getText().length() != 4){
            JOptionPane.showMessageDialog(null, "Title ID not length of 4.");
            return false;
        }
        if (fileBox.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "NDS File Missing.");
            return false;
        } else if(!fileBox.getText().substring(fileBox.getText().lastIndexOf(".")).toLowerCase().equals(".nds")){
            JOptionPane.showMessageDialog(null, "Invalid file selected.");
            return false;
        }
        return true;
    }
    
    private void enableInput(boolean enable) {
        titleKeyBox.setEnabled(enable);
        commonKeyBox.setEnabled(enable);
        gameSelecter.setEnabled(enable);
        gameTitleBox.setEnabled(enable);
        productCodeBox.setEnabled(enable);
        titleIdBox.setEnabled(enable);
        startButton.setEnabled(enable);
        fileBox.setEnabled(enable);
        fileSelectButton.setEnabled(enable);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Metal".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainProgram.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>s
        
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new MainProgram().setVisible(true);
        });
        
    }
    private ComboBoxModel getComboModel ()
    {
      return new DefaultComboBoxModel (UltimateJavaNDSInjector.GAMES.toArray());
    }

    private void incrementProgressBar(String Text) throws InterruptedException{
        progressBar.setString(Text);
        progressBar.setValue(progressBar.getValue()+1);
        lastState++;
        //Thread.sleep(100);
    }
    private void resetProgressBar(String result){
        progressBar.setString(result);
        progressBar.setValue(0);
    }
    private String getWiiUCommonKey()
    {
        String key = "";
        try {
            key = new String(readAllBytes(get("Storage/COMMONKEY"))).substring(0, 32);
        } catch (IOException ex) {
            //doesn't exist yet
        }
        return key;
    }
    
    private void saveWiiUCommonKey() {
        if (!commonKeyBox.getText().equals(getWiiUCommonKey())) {
            try(  PrintWriter out = new PrintWriter( "Storage/COMMONKEY" )  ){
                out.println( commonKeyBox.getText() );
            } catch (FileNotFoundException ex) {
                Logger.getLogger(MainProgram.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void saveTitleKey(GameData game) {
        if (!titleKeyBox.getText().equals(game.getTitleKey())) {
            File dir = new File("Storage");
            if (!(dir.exists() && dir.isDirectory())) {
                dir.mkdir();
            }
            try(  PrintWriter out = new PrintWriter( "Storage/" + game.getTitleKeyFileName() )  ){
                out.println( titleKeyBox.getText() );
            } catch (FileNotFoundException ex) {
                Logger.getLogger(MainProgram.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel baseRomLabel;
    private javax.swing.JLabel bootDrcIcon;
    private javax.swing.JLabel bootDrcImport;
    private javax.swing.JLabel bootDrcLabel;
    private javax.swing.JLayeredPane bootDrcTexPanel;
    private javax.swing.JLabel bootTVIcon;
    private javax.swing.JLabel bootTvImport;
    private javax.swing.JLabel bootTvLabel;
    private javax.swing.JLayeredPane bootTvTexPanel;
    private javax.swing.JTextField commonKeyBox;
    private javax.swing.JLabel commonKeyLabel;
    private javax.swing.JPanel controls;
    private javax.swing.JButton expandButton;
    private javax.swing.JTextField fileBox;
    private javax.swing.JButton fileSelectButton;
    private javax.swing.JPanel gameInfoPanel;
    private javax.swing.JComboBox<UltimateJavaNDSInjector.GameData> gameSelecter;
    private javax.swing.JTextField gameTitleBox;
    private javax.swing.JLabel gameTitleLabel;
    private javax.swing.JLabel iconTexIcon;
    private javax.swing.JLabel iconTexImport;
    private javax.swing.JLayeredPane iconTexPanel;
    private javax.swing.JButton importButton;
    private javax.swing.JButton importButton1;
    private javax.swing.JButton openFolderButton;
    private javax.swing.JTextField productCodeBox;
    private javax.swing.JLabel productCodeLabel;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JButton repackButton;
    private javax.swing.JButton resumeButton;
    private javax.swing.JButton startButton;
    private javax.swing.JTabbedPane tabbedPanel;
    private javax.swing.JFrame textureFrame;
    private javax.swing.JTextField titleIdBox;
    private javax.swing.JLabel titleIdLabel;
    private javax.swing.JTextField titleKeyBox;
    private javax.swing.JLabel titleKeyLabel;
    // End of variables declaration//GEN-END:variables
}
